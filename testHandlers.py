#!/usr/bin/env python

import sys
import collectRunResults
import argparse


def main():

    description = """Test handlers: you need to set a directory with job results and a list of handlers"""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-r', '--results', default=".",
                        help='Directory which contains results, default is the current directory')
    parser.add_argument("-l", "--list-handlers",
                        dest="handlers", help="The list of handlers (comma separated.)",
                        required=True)
    options = parser.parse_args()
    params = []
    params += ['--app-name', 'DummyApp']
    params += ['--app-version', 'v0r0']
    params += ['--app-version-datetime', '2016-01-01 00:00:00 +0200']
    params += ['--exec-name', 'dummy-exec']
    params += ['--exec-content', 'dummy-exec-content']
    params += ['--opt-name', 'dummy-opt']
    params += ['--opt-content', 'dummy-opt-content']
    params += ['-s', '2016-01-01 00:00:00 +0200']
    params += ['-e', '2016-01-01 01:00:00 +0200']
    params += ['-p', 'dummy-host']
    params += ['-c', 'dummy-platform']

    sys.argv += params
    collectRunResults.main()

if __name__ == '__main__':
    main()
